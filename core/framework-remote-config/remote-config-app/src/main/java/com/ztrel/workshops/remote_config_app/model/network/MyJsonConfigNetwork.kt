package com.ztrel.workshops.remote_config_app.model.network

import com.google.gson.annotations.SerializedName


data class MyJsonConfigNetwork(
    @SerializedName("boolean_flag")
    val booleanFlag: Boolean? = null,

    @SerializedName("counter")
    val counter: Int? = null,

    @SerializedName("message")
    val message: String? = null
)
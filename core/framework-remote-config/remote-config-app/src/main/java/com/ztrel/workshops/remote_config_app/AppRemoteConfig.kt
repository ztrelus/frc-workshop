package com.ztrel.workshops.remote_config_app

import com.ztrel.workshops.core_remote_config.IRemoteConfig
import com.ztrel.workshops.remote_config_app.model.MyJsonConfig


interface AppRemoteConfig : IRemoteConfig {

    fun getLoadingMessage(): String

    fun getMyJsonConfig(): MyJsonConfig

    fun getWorkshopConditionalValue(): String

}
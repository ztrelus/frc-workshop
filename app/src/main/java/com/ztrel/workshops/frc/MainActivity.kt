package com.ztrel.workshops.frc

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.analytics.FirebaseAnalytics
import com.ztrel.workshops.remote_config_app.AppRemoteConfig
import com.ztrel.workshops.remote_config_app.AppRemoteConfigImpl
import com.ztrel.workshops.remote_config_app.model.mapping.MyJsonConfigConverter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val config: AppRemoteConfig by lazy {
        AppRemoteConfigImpl(
            MyJsonConfigConverter()
        )
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        FirebaseAnalytics.getInstance(this)
            .setUserProperty("workshop_value", "custom")

        

        config.forceUpdate()
        activity_main_button_action.setOnClickListener {
            Toast.makeText(this, config.getWorkshopConditionalValue(), Toast.LENGTH_LONG)
                .show()
        }
    }
}

package com.ztrel.workshops.remote_config_app.model


data class MyJsonConfig(
    val booleanFlag: Boolean,
    val counter: Int,
    val message: String
) {

    companion object {

        val EMPTY = MyJsonConfig(
            booleanFlag = false,
            counter = 0,
            message = ""
        )

    }

}
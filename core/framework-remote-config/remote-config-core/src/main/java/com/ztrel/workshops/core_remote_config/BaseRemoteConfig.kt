package com.ztrel.workshops.core_remote_config

import android.util.Log
import androidx.annotation.XmlRes
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.google.gson.Gson
import java.util.concurrent.TimeUnit


abstract class BaseRemoteConfig : IRemoteConfig {

    companion object {
        private const val LOG_TAG = "BaseRemoteConfig"

        private val MINIMUM_FETCH_INTERVAL_IN_SECONDS = TimeUnit.HOURS.toSeconds(12)
    }


    private val config by lazy {
        FirebaseRemoteConfig.getInstance().apply {
            val configSettings = FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(if (BuildConfig.DEBUG) 0L else MINIMUM_FETCH_INTERVAL_IN_SECONDS)
                .build()

            setConfigSettingsAsync(configSettings)
                .addOnSuccessListener { Log.i(LOG_TAG, "Set remote config settings success") }
                .addOnFailureListener { Log.e(LOG_TAG, "Set remote config settings failure", it) }

            setDefaultsAsync(getDefaultRemoteConfigValuesXmlResource())
                .addOnSuccessListener { Log.i(LOG_TAG, "Set remote config defaults success") }
                .addOnFailureListener { Log.e(LOG_TAG, "Set remote config defaults failure", it) }
        }
    }


    protected val fetcher by lazy {
        RemoteConfigValuesFetcher(
            gson = Gson(),
            config = config
        )
    }


    @XmlRes
    abstract fun getDefaultRemoteConfigValuesXmlResource(): Int


    override fun forceUpdate() {
        fetchAndActivate()
    }

    fun fetchAndActivate() {
        config.fetchAndActivate()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val updated = task.result
                    Log.i(LOG_TAG, "Remote config params update: $updated")
                } else {
                    Log.e(LOG_TAG, "Remote config params update failure", task.exception)
                }
            }
    }

}
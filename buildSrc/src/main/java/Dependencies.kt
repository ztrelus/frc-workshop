object Versions {

    // Remember that "kotlin" && "androidTools" versions duplicated
    // in build.gradle of buildSrc module.

    const val kotlinVersion = "1.3.61"
    const val androidToolsVersion = "3.5.3"
    const val buildToolsVersion = "29.0.2"
    const val minSdkVersion = 21

    const val targetSdkVersion = 29
    const val compileSdkVersion = 29
}

object Libs {

    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlinVersion}"

    val androidX = AndroidXLibs
    val firebase = FirebaseLibs
    val network = NetworkLibs


    object AndroidXLibs {

        private const val androidXVersion = "1.0.2"
        private const val constraintLayoutVersion = "1.1.3"

        const val appCompat = "androidx.appcompat:appcompat:$androidXVersion"
        const val coreKtx = "androidx.core:core-ktx:$androidXVersion"
        const val constraintLayout = "androidx.constraintlayout:constraintlayout:$constraintLayoutVersion"
    }

    object FirebaseLibs {

        private const val analyticsVersion = "17.2.2"
        private const val remoteConfigVersion = "19.1.2"


        const val analytics = "com.google.firebase:firebase-analytics:$analyticsVersion"
        const val remoteConfig = "com.google.firebase:firebase-config:$remoteConfigVersion"

    }

    object NetworkLibs {
        private const val gsonVersion = "2.8.6"

        const val gson = "com.google.code.gson:gson:$gsonVersion"
    }

}

object Plugins {

    private const val googleServicesPluginVersion = "4.3.3"

    const val androidTools = "com.android.tools.build:gradle:${Versions.androidToolsVersion}"
    const val kotlin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlinVersion}"
    const val googleServices = "com.google.gms:google-services:${googleServicesPluginVersion}"

}
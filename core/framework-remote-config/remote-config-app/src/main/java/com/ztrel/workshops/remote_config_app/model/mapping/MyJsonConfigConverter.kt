package com.ztrel.workshops.remote_config_app.model.mapping

import com.ztrel.workshops.remote_config_app.model.MyJsonConfig
import com.ztrel.workshops.remote_config_app.model.network.MyJsonConfigNetwork


class MyJsonConfigConverter {

    fun convert(item: MyJsonConfigNetwork): MyJsonConfig {
        return with(item) {
            MyJsonConfig(
                booleanFlag = booleanFlag ?: false,
                counter = counter ?: 0,
                message = message ?: ""
            )
        }
    }

}
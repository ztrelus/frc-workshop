package com.ztrel.workshops.remote_config_app

import com.ztrel.workshops.core_remote_config.BaseRemoteConfig
import com.ztrel.workshops.remote_config_app.model.MyJsonConfig
import com.ztrel.workshops.remote_config_app.model.mapping.MyJsonConfigConverter
import com.ztrel.workshops.remote_config_app.model.network.MyJsonConfigNetwork


class AppRemoteConfigImpl(
    private val myJsonConfigConverter: MyJsonConfigConverter
) : BaseRemoteConfig(), AppRemoteConfig {

    companion object {
        private const val KEY_LOADING_MESSAGE = "loading_message"
        private const val KEY_MY_JSON_CONFIG = "my_json_config"
        private const val KEY_WORKSHOP_CONDITIONAL_VALUE = "workshop_conditional_value"
    }


    override fun getDefaultRemoteConfigValuesXmlResource(): Int = R.xml.default_app_remote_config


    override fun getLoadingMessage(): String {
        return fetcher.getString(KEY_LOADING_MESSAGE)
    }

    override fun getMyJsonConfig(): MyJsonConfig {
        return fetcher.getJsonObjectValue(
            key = KEY_MY_JSON_CONFIG,
            networkModelClass = MyJsonConfigNetwork::class.java,
            converter = { myJsonConfigConverter.convert(it) },
            defaultValue = { MyJsonConfig.EMPTY }
        )
    }

    override fun getWorkshopConditionalValue(): String {
        return fetcher.getString(KEY_WORKSHOP_CONDITIONAL_VALUE)
    }

}
package com.ztrel.workshops.core_remote_config

import android.util.Log
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.gson.Gson


class RemoteConfigValuesFetcher(
    private val gson: Gson,
    private val config: FirebaseRemoteConfig
) {

    companion object {
        private const val LOG_TAG = "RemoteConfigValuesFet"
    }

    fun getDouble(key: String): Double = config.getDouble(key)

    fun getBoolean(key: String): Boolean = config.getBoolean(key)

    fun getString(key: String): String = config.getString(key)

    fun getLong(key: String): Long = config.getLong(key)


    fun <NetworkModel, DomainModel> getJsonObjectValue(
        key: String,
        networkModelClass: Class<NetworkModel>,
        converter: (NetworkModel) -> DomainModel,
        defaultValue: () -> DomainModel
    ): DomainModel {
        val networkModelJson = getNetworkModelJson(key) ?: return defaultValue.invoke()

        return try {
            val networkModel = gson.fromJson<NetworkModel?>(networkModelJson, networkModelClass)

            networkModel?.run(converter) ?: defaultValue.invoke()
        } catch (ex: Exception) {
            Log.e(
                LOG_TAG,
                "Error with parsing json property from remote config [key: $key, networkJson: $networkModelJson]",
                ex
            )
            defaultValue.invoke()
        }
    }


    private fun getNetworkModelJson(key: String): String? {
        return try {
            config.getString(key)
        } catch (ex: Exception) {
            Log.e(LOG_TAG, "Error with fetching json property from remote config [key: $key]", ex)
            null
        }
    }

}